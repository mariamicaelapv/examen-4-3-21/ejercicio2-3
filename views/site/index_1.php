<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Consultas de seleccion examen';
?>
<div class="site-about">

    <div class="jumbotron">
        <h1>Consultas de Seleccion examen</h1>
    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1</h3>
                        <p> el numero total de km recorridos por los ciclistas con active record
                      
            </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1a'], ['class' => 'btn btn-primary']) ?>
                            
                        </p>    
                    </div>
                </div>
            </div>
            
            
               <div class="body-content">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 2</h3>
                        <p> los nombres Y dorsales de los ciclistas que habiendo ganado etapas, nunca han llevado el maillot de color rosa con dao
                        </p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta2'], ['class' => 'btn btn-primary']) ?>
                            
                        </p>    
                    </div>
                </div>
            </div>