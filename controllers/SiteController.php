<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use app\models\Puerto;
use app\models\Etapa;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    
    {
        return $this->render('index_1');
    }
      public function actionConsulta1a() {
        //mediante active record
         $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                ->select("sum(kms)"),
                
                'pagination' => [
                'pageSize' => 5,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['kms'],
                    "titulo" => "Consulta 3 con Active Record",
                    "enunciado" => "El numero total de kms recorridos por los ciclistas",
                    "sql" => "SELECT sum(kms) from etapa   Los pasos que he seguido para hacer esta consulta es sumando los kms totales que han recorrido ",
        ]);
        
        
        
    }
    
     public function actionConsulta2() {
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"",
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Dorsal'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Los nombres Y dorsales de los ciclistas que habiendo ganado etapas, nunca han llevado el maillot de color rosa con dao",
            "sql"=>"",
            ]);
}

   
    
    
    
    
    
    
    
}
